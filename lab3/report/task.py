import cv2

img1 = cv2.imread(r'E:\ferit\osirv_20_21\lab3\slike\airplane.bmp')
img2 = cv2.imread(r'E:\ferit\osirv_20_21\lab3\slike\baboon.bmp')
img3 = cv2.imread(r'E:\ferit\osirv_20_21\lab3\slike\boats.bmp')

for img in (img1,img2,img3):
    ret, threshold= cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)
    cv2.imwrite(str(img) + 'binary.jpg', threshold)
    ret, threshold= cv2.threshold(img,127, 255, cv2.THRESH_TOZERO_INV)
    cv2.imwrite(str(img)+ 'tozero_inv.jpg', threshold)