import cv2
import numpy as np
from math import pi

img_path='lab2/slike/baboon.bmp'
img=cv2.imread(img_path,cv2.IMREAD_GRAYSCALE)

rotated_imgs=[]

for rotation_deg in range(0,360,30):
    print(rotation_deg)
    rotation_radian= rotation_deg * pi/180
    print(rotation_radian)
    img_copy=img.copy()
    img_copy=cv2.resize(img_copy, None, fx=0.25, fy=0.25, interpolation=cv2.INTER_CUBIC)

    rows,cols=img_copy.shape
    M=cv2.getRotationMatrix2D((cols/2,rows/2),rotation_deg,1)
    img_copy=cv2.warpAffine(img_copy,M,(cols,rows))

    rotated_imgs.append(img_copy)

final_img=np.hstack(tuple(rotated_imgs))
cv2.imshow('img', final_img)
cv2.waitKey(0)
cv2.destroyAllWindows()