import os
import cv2

src_dir_path='lab2/slike'
dst_dir_path='lab2/problem3'
os.makedirs(dst_dir_path, exist_ok=True)

for img_name in os.listdir(src_dir_path):
    img_path=os.path.join(src_dir_path,img_name)
    img=cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
    img_inverted=cv2.bitwise_not(img)
    img_inverted_dst_path=os.path.join(dst_dir_path,img_name.split('.')[0]+"_invert.jpg")
    print(img_inverted_dst_path)
    cv2.imwrite(img_inverted_dst_path,img_inverted)