import os
import cv2

src_dir_path='lab2/slike'
dst_dir_path='lab2/problem4'
os.makedirs(dst_dir_path, exist_ok=True)

for threshold in [63, 127, 191]:
    for img_name in os.listdir(src_dir_path):
        img_path=os.path.join(src_dir_path,img_name)
        img=cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        img_threshold=  img * (img>threshold)
        img_threshold_dst_path=os.path.join(dst_dir_path,img_name.split('.')[0]+f"_{threshold}_thresh.jpg")
        print(img_threshold_dst_path)
        cv2.imwrite(img_threshold_dst_path,img_threshold)