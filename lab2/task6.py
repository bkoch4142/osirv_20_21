import cv2
import os
import numpy as np

img_path='lab2/slike/BoatsColor.bmp'
img=cv2.imread(img_path,cv2.IMREAD_GRAYSCALE)

print(np.max(img), np.min(img))
img=img.astype(np.float32)
print(np.max(img), np.min(img))
img[img>255]=255
img[img<0]=0
print(np.max(img), np.min(img))

os.makedirs('lab2/problem6',exist_ok=True)
for q in range(1,9):
    dst_path=os.path.join('lab2/problem6', f"boats_{q}.bmp")
    d=pow(2,8-q)
    print(d)
    img_copy=img.copy()

    noise=np.random.uniform(0,1,img_copy.shape)

    img_copy= (np.floor(img_copy/d + noise)+1/2)*d

    img_copy=img_copy.astype(np.uint8)
    cv2.imwrite(dst_path,img_copy)