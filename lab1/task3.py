import cv2
import os

dst='lab1/output'
img=cv2.imread('lab1/slike/airplane.bmp', 1)

img_border=cv2.copyMakeBorder(img,top=10,bottom=10,left=10,right=10,borderType=cv2.BORDER_CONSTANT)
cv2.imwrite(os.path.join(dst,'slika_obrub.jpg'),img_border)