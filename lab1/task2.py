import cv2
import os

dst='lab1/output'
img=cv2.imread('lab1/slike/airplane.bmp', 1)

blue=img.copy()
blue[:,:,(1,2)]*=0

green=img.copy()
green[:,:,(0,2)]*=0

red=img.copy()
red[:,:,(0,1)]*=0

cv2.imshow('plava.jpg', blue)
cv2.imshow('zelena.jpg', green)
cv2.imshow('crvena.jpg', red)
cv2.waitKey(0)
cv2.destroyAllWindows()
