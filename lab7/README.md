# Lab 7: Color Spaces and OpenCV GUI Functions

In this lab, you'll get familiar with image color spaces. On the web and in general usage, most images are encoded as **RGB**: **R**ed, **G**reen, and **B**lue. OpenCV generally uses **BGR**: Blue, Green, Red.

This is just one of the many ways we can represent an image. In an RGB image, we get a pixel by mixing the three colors. We can get the same pixel by using different numbers and formulae to combine them. For instance, the **CMYK** color space encodes each pixel in 4 primary colors: **C**yan, **M**agenta, **Y**ellow and **K**ey (Black). Since printers use these primary colors, CMYK is often used when preparing images for print.

Not all color spaces consist only of primary colors. For instance, **HSV** (**H**ue, **S**aturation, **V**alue) stores the color in Hue, the color's intensity in Saturation, and the general brightness of that pixel in Value. The Hue portion is a number in [0, 179] (in OpenCV, usually it's an arc around a circle, so [0, 360)) where 0 is red, and the hue slowly shifts to green and then blue as you get to higher numbers.

![](hsv_1.png)

You can think of the whole HSV color space as a cylinder. The height on the cylinder corresponds to how dark the pixel is, the distance from the center tells you how non-gray it is, and the angle tells you which color the pixel is.

![](hsv_2.png)

*(source: https://en.wikipedia.org/wiki/HSL_and_HSV#/media/File:HSV_color_solid_cylinder_saturation_gray.png)*

There are many color spaces each with its uses. One other color space we'll mention in **YCbCr**. Y is the **luma** component, similar to the Value in HSV. Cb is the **blue-difference chroma component**, i.e. how blue should this pixel be tinted. Similarly, Cr is the **red-difference chroma component**, which tells you how much should a pixel be tinted red. Even with a different type of representation, each YCbCr is capable of showing all RGB images.

![](ycbcr.png)

*(source: https://en.wikipedia.org/wiki/YCbCr#/media/File:CCD.png)*

The reason YCbCr is important is because of the human eye. Our eyes are much more sensitive to luminance than actual color differences. Therefore, when compressing images, it's better to compress the chroma components than luminance if you want the image to look the same to a human observer.  This is called **chroma subsampling** and is used heavily in image and video compression, including MPEG, JPEG, DVD and Blu-Rays, and many others.

## In OpenCV

OpenCV supports a plethora of color spaces for images. The main function to convert color spaces is: [img = cv.cvtColor(img, code)](https://docs.opencv.org/master/d8/d01/group__imgproc__color__conversions.html#ga397ae87e1288a81d2363b61574eb8cab). The `code` tells OpenCV **from** which format to convert the image, as well as **to** which format. You can see all the color conversion codes [here](https://docs.opencv.org/master/d8/d01/group__imgproc__color__conversions.html#ga4e0972be5de079fed4e3a10e24ef5ef0). The main ones you'll use are:

- cv.COLOR_BGR2YCrCb (BGR to YCbCr)
- cv.COLOR_YCrCb2BGR (back to BGR as the name suggests)
- cv.COLOR_BGR2HSV
- etc.

Note: You'll have to convert the image back to BGR if you want to use `cv.imshow()` to display it in its original form.

### OpenCV GUIs

You already used `cv.imshow()` to show a window from OpenCV that shows an image. OpenCV windows can also receive user input (useful for drawing, etc) and even show trackbars (sliders) to select colors, threshold values, a frame of video, and similar.

To use GUI features like a trackbar you have to first **create a named window** with a unique name:

```python
cv.namedWindow("Trackbar Window")
```

You can then use different OpenCV GUI functions with that name to add features to that window.

```python
cv.createTrackbar("Hue", "Trackbar Window", -100, 100, on_trackbar)
cv.imshow("Trackbar Window", img_copy)
```

## Reading

- https://docs.opencv.org/master/df/d9d/tutorial_py_colorspaces.html
- https://docs.opencv.org/3.4/da/d6a/tutorial_trackbar.html
- https://en.wikipedia.org/wiki/Chrominance
- https://en.wikipedia.org/wiki/Chroma_subsampling

## Assignments

Make sure you opened the Reading links above and got familiar with each. Each assignment has a corresponding file named **assignmentN.py**. Open the files and add code where you see `# TODO` comments. Reading the TODO explains what you'll have to do. Refer to the Reading portion above to more easily find solutions.

### Assignment 1

In this assignment, you'll build an OpenCV window that includes a trackbar which the user can use to shift the hue of an image. You'll do this by converting the image into HSV, and then adding the trackbar value to the hue channel of the image. You'll then convert it back to BGR and show it on the window.

This is what your result should look like:

![](example_1.png)

### Assignment 2

In the second assignment, you'll threshold the peppers image to only show green peppers. You do this by converting the image to HSV and then using [`cv.inRange()`](https://docs.opencv.org/3.4/d2/de8/group__core__array.html#ga48af0ab51e36436c5d04340e036ce981) to get a mask of pixels that are inside the green-ish hue range. You'll then use [`cv.bitwise_and()`](https://docs.opencv.org/master/d2/de8/group__core__array.html#ga60b4d04b251ba5eb1392c34425497e14) to apply the mask to the original image to display only the green pixels. Your result should look similar (but not necessarily identical) to this:

![](example_2.png)

### Assignment 3

In the final assignment, you'll perform chroma subsampling. First, you'll subsample all three channels of a BGR image and show the result. Then, you'll convert the image to YCbCr and subsample only the Cb and Cr channels, but you'll apply heavier subsampling than in the BGR image. You'll then calculate the size of your subsampled image and compare the image quality and its size with the BGR example.

The point of the subsampling is to reduce the bit depth of an image. You are already familiar with the fact that images usually take up 3 (channels) * 8 bits per pixel, with three [0, 255] values. By reducing the bit depth to 7, each pixel takes up only 3 * 7 bits, leading to a smaller image size but also a reduced number of possible color values for each channel, 2^7 or 128, as opposed to a bit depth of 8 which shows 2^8 or 256 different values for each channel.

Note: The "subsampling" "algorithm" used in this assignment is not a good subsampling method, it's just there to visually show what subsampling could look like.